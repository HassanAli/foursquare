package com.android.common_test

import com.android.common.error.ErrorCode
import com.android.common.error.ErrorThrowable
import com.android.data.entity.model.remote.list.Venue
import com.android.data.entity.model.remote.list.Venues
import com.google.gson.Gson

/**
 * Created by hassanalizadeh on 30,August,2020
 */
object TestUtil {

    fun firstVenueFromRemote(): Venue {
        return venuesFromRemote()
            .response
            .groups
            ?.get(0)
            ?.items
            ?.get(0)
            ?.venue!!
    }

    val location: Pair<Double, Double> = 35.7833944 to 51.3787218

    fun venuesFromRemote(): Venues {
        return Gson().fromJson(parseJson("venues.json"), Venues::class.java)
    }

    private fun parseJson(fileName: String): String =
        javaClass.classLoader?.getResourceAsStream("json/$fileName")
            ?.bufferedReader().use { it?.readText().orEmpty() }

    fun error(): ErrorThrowable = ErrorThrowable(ErrorCode.ERROR_HAPPENED)
}