package com.android.data.repository.datasource.venue

import com.android.common.error.ErrorThrowable
import com.android.common_test.TestUtil
import com.android.data.entity.dao.VenueDao
import com.android.data.network.DataServiceVenues
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

/**
 * Created by hassanalizadeh on 30,August,2020
 */
@RunWith(JUnit4::class)
class SmartVenuesDataSourceTest {

    @Mock
    private lateinit var dataService: DataServiceVenues

    @Mock
    private lateinit var venueDao: VenueDao
    private lateinit var smartVenuesDataSource: SmartVenuesDataSource


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        smartVenuesDataSource = spy(
            SmartVenuesDataSource(
                dataService,
                venueDao
            )
        )
    }

    @Test
    fun `get venues`() {
        // GIVEN
        doReturn(Flowable.just(mutableListOf(TestUtil.firstVenueFromRemote())))
            .whenever(venueDao)
            .selectAll()

        // WHEN
        smartVenuesDataSource.venues()
            .test()
            .assertComplete()

        // THEN
        verify(venueDao).selectAll()
    }

    @Test
    fun `load venues with result`() {
        // GIVEN
        val expectedPage = 1
        val expectedTotalCount = TestUtil.venuesFromRemote().response.totalResults
        doReturn(Single.just(TestUtil.venuesFromRemote())).whenever(dataService).venues(anyMap())

        // WHEN
        smartVenuesDataSource.loadVenues(TestUtil.location)
            .test()
            .assertComplete()

        // THEN
        verify(dataService).venues(
            argThat {
                this[offset] == expectedPage.toString() &&
                        this[ll] == ("${TestUtil.location.first},${TestUtil.location.second}")
            })
        verify(venueDao).deleteAll()
        verify(venueDao).insert(
            argThat {
                this.size == 30 && this[0].id == TestUtil.firstVenueFromRemote().id
            }
        )
        assert(smartVenuesDataSource._totalCount == expectedTotalCount)
    }

    @Test
    fun `load venues on error`() {
        // GIVEN
        doReturn(Single.error<ErrorThrowable>(TestUtil.error()))
            .whenever(dataService).venues(anyMap())

        // WHEN
        smartVenuesDataSource.loadVenues(TestUtil.location)
            .test()
            .assertNotComplete()

        // THEN
        verify(venueDao, never()).insert(anyList())
    }

    @Test
    fun `load more with result`() {
        // GIVEN
        val page = 3
        doReturn(Single.just(TestUtil.venuesFromRemote())).whenever(dataService).venues(anyMap())

        // WHEN
        smartVenuesDataSource.loadMoreVenues()
            .test()
            .assertComplete()
        smartVenuesDataSource.loadMoreVenues()
            .test()
            .assertComplete()

        // THEN
        verify(dataService).venues(
            argThat {
                this[offset].equals(1.toString())
            })
    }

    companion object {
        const val offset = "offset"
        const val ll = "ll"
    }

}