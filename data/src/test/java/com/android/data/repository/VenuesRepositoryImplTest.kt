package com.android.data.repository

import com.android.common_test.TestUtil
import com.android.data.entity.mapper.map
import com.android.data.repository.datasource.venue.VenuesDataSource
import com.android.domain.repository.VenuesRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Created by hassanalizadeh on 30,August,2020
 */
@RunWith(JUnit4::class)
class VenuesRepositoryImplTest {

    @Mock
    private lateinit var dataSource: VenuesDataSource
    private lateinit var repository: VenuesRepository


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = VenuesRepositoryImpl(dataSource)
    }

    @Test
    fun `get venues`() {
        //GIVEN
        doReturn(Flowable.just(Pair(0, TestUtil.venuesFromRemote().map())))
            .whenever(dataSource).venues()

        //WHEN
        repository.venues()
            .test()
            .assertValue {
                it.venues[0].id == TestUtil.firstVenueFromRemote().id
            }
            .assertComplete()

        //THEN
        verify(dataSource).venues()
    }

    @Test
    fun `load venues`() {
        //GIVEN
        doReturn(Completable.complete()).whenever(dataSource).loadVenues(TestUtil.location)

        //WHEN
        repository.loadVenues(any())
            .test()
            .assertComplete()

        //THEN
        verify(dataSource).loadVenues(any())
    }

    @Test
    fun `load more venues`() {
        //GIVEN
        doReturn(Completable.complete()).whenever(dataSource).loadMoreVenues()

        //WHEN
        repository.loadMoreVenues()
            .test()
            .assertComplete()

        //THEN
        verify(dataSource).loadMoreVenues()
    }

}