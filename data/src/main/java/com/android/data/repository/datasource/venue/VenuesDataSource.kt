package com.android.data.repository.datasource.venue

import com.android.data.entity.model.local.VenueEntity
import com.android.data.entity.model.remote.detail.Detail
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 29,August,2020
 */
interface VenuesDataSource {
    fun venues(): Flowable<Pair<Int, List<VenueEntity>>>
    fun loadVenues(param: Pair<Double, Double>): Completable
    fun loadMoreVenues(): Completable
    fun detail(id: String): Single<Detail>
}