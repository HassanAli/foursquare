package com.android.data.repository.datasource.venue

import androidx.annotation.VisibleForTesting
import com.android.data.entity.dao.VenueDao
import com.android.data.entity.mapper.map
import com.android.data.entity.model.local.VenueEntity
import com.android.data.entity.model.remote.detail.Detail
import com.android.data.extension.onError
import com.android.data.network.DataServiceVenues
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 29,August,2020
 */
open class SmartVenuesDataSource @Inject constructor(
    private val service: DataServiceVenues,
    private val venueDao: VenueDao
) : VenuesDataSource {

    @Volatile
    private var _page: Int = 1
    @Volatile
    private var latitude: Double = 0.0
    @Volatile
    private var longitude: Double = 0.0

    @VisibleForTesting
    @Volatile
    var _totalCount: Int = 0


    override fun venues(): Flowable<Pair<Int, List<VenueEntity>>> {
        return venueDao.selectAll()
            .map {
                return@map if (_totalCount == 0)
                    it.size to it
                else
                    _totalCount to it
            }
            .onError()
    }

    override fun loadVenues(param: Pair<Double, Double>): Completable {
        return resetPage()
            .andThen(setLocation(param))
            .andThen(getQueryParams())
            .flatMap { service.venues(it) }
            .flatMap { setTotalCount(it.response.totalResults).toSingle { it } }
            .flatMap { clearVenues().toSingle { it } }
            .flatMapCompletable { insertVenues(it.map()) }
            .onError()
    }

    override fun loadMoreVenues(): Completable {
        return getQueryParams()
            .flatMap { service.venues(it) }
            .flatMap { increasePage().toSingle { it } }
            .flatMapCompletable { insertVenues(it.map()) }
            .onError()
    }

    override fun detail(id: String): Single<Detail> {
        return service.detail(id)
            .onError()
    }

    private fun insertVenues(venues: List<VenueEntity>?): Completable {
        if (venues.isNullOrEmpty()) return Completable.complete()
        return Completable.fromAction { venueDao.insert(venues) }
            .onError()
    }

    private fun clearVenues(): Completable {
        return Completable.fromAction { venueDao.deleteAll() }
    }

    private fun setTotalCount(totalCount: Int): Completable {
        return Completable.fromAction { this._totalCount = totalCount }
    }

    private fun getQueryParams(): Single<MutableMap<String, String>> {
        return Single.just(mutableMapOf<String, String>())
            .map {
                it["offset"] = _page.toString()
                it["ll"] = "$latitude,$longitude"
                return@map it
            }.onError()
    }

    private fun setLocation(location: Pair<Double, Double>): Completable {
        return Completable.fromAction {
            this.latitude = location.first
            this.longitude = location.second
        }
    }

    private fun resetPage(): Completable {
        return Completable.fromAction {
            _page = 1
        }
    }

    private fun increasePage(): Completable {
        return Completable.fromAction {
            _page += 1
        }
    }

}