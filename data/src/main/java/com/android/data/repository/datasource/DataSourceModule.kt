package com.android.data.repository.datasource

import com.android.data.repository.datasource.venue.SmartVenuesDataSource
import com.android.data.repository.datasource.venue.VenuesDataSource
import dagger.Binds
import dagger.Module
import dagger.Reusable

/**
 * Created by hassanalizadeh on 29,August,2020
 */
@Module
abstract class DataSourceModule {

    @Binds
    @Reusable
    abstract fun venuesDataSource(smartVenuesDataSource: SmartVenuesDataSource): VenuesDataSource

}