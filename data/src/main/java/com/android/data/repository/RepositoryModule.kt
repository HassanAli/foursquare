package com.android.data.repository

import com.android.data.repository.datasource.DataSourceModule
import com.android.domain.repository.VenuesRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by hassanalizadeh on 29,August,2020
 */
@Module(includes = [DataSourceModule::class])
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun venuesRepository(
        venuesRepositoryImpl: VenuesRepositoryImpl
    ): VenuesRepository

}