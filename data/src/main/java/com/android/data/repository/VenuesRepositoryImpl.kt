package com.android.data.repository

import com.android.data.entity.mapper.map
import com.android.data.repository.datasource.venue.VenuesDataSource
import com.android.domain.entity.DetailObject
import com.android.domain.entity.VenuesObject
import com.android.domain.repository.VenuesRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 29,August,2020
 */
class VenuesRepositoryImpl @Inject constructor(
    private val dataSource: VenuesDataSource
) : VenuesRepository {

    override fun venues(): Flowable<VenuesObject> {
        return dataSource.venues().map {
            return@map VenuesObject(it.first, it.second.map())
        }
    }

    override fun loadVenues(param: Pair<Double, Double>): Completable {
        return dataSource.loadVenues(param)
    }

    override fun loadMoreVenues(): Completable {
        return dataSource.loadMoreVenues()
    }

    override fun detail(id: String): Single<DetailObject> {
        return dataSource.detail(id).map { it.map() }
    }

}