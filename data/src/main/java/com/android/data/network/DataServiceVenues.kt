package com.android.data.network

import com.android.data.entity.model.remote.detail.Detail
import com.android.data.entity.model.remote.list.Venues
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

/**
 * Created by hassanalizadeh on 28,August,2020
 */
interface DataServiceVenues {

    @GET("venues/explore")
    fun venues(
        @QueryMap map: Map<String, String>
    ): Single<Venues>

    @GET(value = "venues/{id}")
    fun detail(
        @Path("id") venueId: String
    ): Single<Detail>

}