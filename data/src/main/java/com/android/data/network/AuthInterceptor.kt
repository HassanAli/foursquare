package com.android.data.network

import okhttp3.Interceptor
import okhttp3.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by hassanalizadeh on 28,August,2020
 */
class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val formatter = SimpleDateFormat("YYYYMMDD", Locale.getDefault())

        val url = request.url.newBuilder()
            .addQueryParameter("client_id", "T13LNQF1PROPLE1TOZSYOY15P1AUXS3W3BKDE0W1ZOPBVNNI")
            .addQueryParameter("client_secret", "JJ0J5DJOGWZH04X5MS2HGVOU2EQNYR4RJOSBY5CTXXXEDCRU")
            .addQueryParameter("v", formatter.format(Date()))
            .addQueryParameter("radius", "1000")
            .build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}