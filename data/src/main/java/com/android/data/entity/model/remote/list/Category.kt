package com.android.data.entity.model.remote.list

data class Category(
	val name: String,
	val icon: Icon
)