package com.android.data.entity.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.data.entity.model.local.VenueEntity
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 28,August,2020
 */
@Dao
interface VenueDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(venues: List<VenueEntity>)

    @Query("SELECT * FROM venue WHERE id=:id")
    fun selectById(id: Int): Single<VenueEntity>

    @Query("SELECT * FROM venue")
    fun selectAll(): Flowable<List<VenueEntity>>

    @Query("DELETE FROM venue")
    fun deleteAll()

}