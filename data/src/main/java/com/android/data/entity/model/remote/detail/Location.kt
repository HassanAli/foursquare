package com.android.data.entity.model.remote.detail

data class Location(
	val formattedAddress: List<String>
)