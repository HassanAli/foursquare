package com.android.data.entity

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.data.entity.dao.VenueDao
import com.android.data.entity.model.local.VenueEntity

/**
 * The FourSquare's Database.
 */
@Database(
    entities = [VenueEntity::class],
    version = 1,
    exportSchema = false
)
abstract class FourSquareDatabase : RoomDatabase() {

    abstract fun venueDao(): VenueDao

}