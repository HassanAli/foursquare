package com.android.data.entity.mapper

import com.android.data.entity.model.local.CategoryEntity
import com.android.data.entity.model.local.VenueEntity
import com.android.domain.entity.CategoryObject
import com.android.domain.entity.VenueObject

/**
 * Created by hassanalizadeh on 29,August,2020
 */
fun List<VenueEntity>.map(): List<VenueObject> = map { venue -> venue.map() }

fun VenueEntity.map(): VenueObject = VenueObject(
    id = id,
    name = name,
    address = address,
    category = category?.map()
)

fun CategoryEntity.map(): CategoryObject = CategoryObject(
    name = name,
    icon = icon
)