package com.android.data.entity.model.remote.list

data class Response(
	val totalResults: Int,
	val groups: List<Groups>?
)