package com.android.data.entity

import android.app.Application
import androidx.room.Room
import com.android.data.entity.dao.VenueDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Entity provider module.
 */
@Module
class EntityModule {

    @Provides
    fun venueDao(db: FourSquareDatabase): VenueDao = db.venueDao()

    @Provides
    @Singleton
    fun database(application: Application): FourSquareDatabase = Room.databaseBuilder(
        application.applicationContext,
        FourSquareDatabase::class.java,
        "venue_db"
    ).build()

}