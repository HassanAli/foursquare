package com.android.data.entity.model.remote.detail

data class Icon(
	val prefix: String,
	val suffix: String
)