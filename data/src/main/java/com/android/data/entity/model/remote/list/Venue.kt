package com.android.data.entity.model.remote.list

data class Venue(
    val id: String,
    val name: String,
    val location: Location?,
    val categories: List<Category>?
)