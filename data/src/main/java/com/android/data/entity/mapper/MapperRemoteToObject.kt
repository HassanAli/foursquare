package com.android.data.entity.mapper

import com.android.data.entity.model.remote.detail.Detail
import com.android.data.entity.model.remote.list.Category
import com.android.domain.entity.CategoryObject
import com.android.domain.entity.DetailObject

/**
 * Created by hassanalizadeh on 31,August,2020
 */

fun Detail.map(): DetailObject {
    val venue = response.venue

    return DetailObject(
        id = venue.id,
        name = venue.name,
        phone = venue.contact.phone,
        address = venue.location.formattedAddress.joinToString(),
        categories = venue.categories.map { it.mapToObject() },
        verified = venue.verified,
        rating = venue.rating,
        description = venue.description,
        defaultHour = venue.defaultHours?.status
    )
}

fun Category.mapToObject(): CategoryObject = CategoryObject(
    name = name,
    icon = icon.prefix + 32 + icon.suffix
)