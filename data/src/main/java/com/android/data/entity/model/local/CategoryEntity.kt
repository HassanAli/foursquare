package com.android.data.entity.model.local

/**
 * Created by hassanalizadeh on 28,August,2020
 */
data class CategoryEntity(
    val name: String,
    val icon: String?
)