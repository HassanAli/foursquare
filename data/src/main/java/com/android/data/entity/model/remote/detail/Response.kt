package com.android.data.entity.model.remote.detail

data class Response(
	val venue: Venue
)