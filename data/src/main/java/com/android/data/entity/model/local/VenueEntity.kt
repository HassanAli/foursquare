package com.android.data.entity.model.local

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by hassanalizadeh on 28,August,2020
 */
@Entity(tableName = "venue")
data class VenueEntity(
    @PrimaryKey
    val id: String,
    val name: String,
    val address: String?,
    @Embedded(prefix = "category_")
    val category: CategoryEntity?
)