package com.android.data.entity.model.remote.detail

import com.android.data.entity.model.remote.list.Category

data class Venue(
	val id: String,
	val name: String,
	val contact: Contact,
	val location: Location,
	val categories: List<Category>,
	val verified: Boolean,
	val rating: Double,
	val description: String?,
	val defaultHours: DefaultHours?
)