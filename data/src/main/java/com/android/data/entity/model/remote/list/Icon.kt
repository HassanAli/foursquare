package com.android.data.entity.model.remote.list

data class Icon(
	val prefix: String,
	val suffix: String
)