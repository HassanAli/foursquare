package com.android.data.entity.mapper

import com.android.data.entity.model.local.CategoryEntity
import com.android.data.entity.model.local.VenueEntity
import com.android.data.entity.model.remote.list.Category
import com.android.data.entity.model.remote.list.Location
import com.android.data.entity.model.remote.list.Venue
import com.android.data.entity.model.remote.list.Venues

/**
 * Created by hassanalizadeh on 29,August,2020
 */

fun Venues.map(): List<VenueEntity>? =
    response.groups?.get(0)?.items?.map {
        it.venue.map()
    }

fun Venue.map(): VenueEntity = VenueEntity(
    id = id,
    name = name,
    address = location?.map(),
    category = categories?.firstOrNull()?.map()
)

fun Location.map(): String = formattedAddress.joinToString()

fun Category.map(): CategoryEntity = CategoryEntity(
    name = name,
    icon = icon.prefix + 32 + icon.suffix
)