package com.android.data.entity.model.remote.list

data class Location(
	val distance: Int,
	val formattedAddress: List<String>
)