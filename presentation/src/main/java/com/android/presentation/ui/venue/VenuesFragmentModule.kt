package com.android.presentation.ui.venue

import dagger.Module

/**
 * Created by hassanalizadeh on 28,August,2020
 */
@Module(includes = [VenuesViewModelModule::class])
abstract class VenuesFragmentModule