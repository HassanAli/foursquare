package com.android.presentation.ui.venue

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.domain.entity.VenueObject
import com.android.presentation.R
import com.android.presentation.adapter.BaseAction
import com.android.presentation.common.extension.activityViewModelProvider
import com.android.presentation.common.extension.observe
import com.android.presentation.common.extension.viewModelProvider
import com.android.presentation.common.view.BaseFragment
import com.android.presentation.common.viewmodel.ViewModelProviderFactory
import com.android.presentation.location.LocationViewModel
import com.android.presentation.ui.venue.adapter.VenuesAdapter
import com.android.presentation.ui.venue.adapter.ViewVenueAction
import kotlinx.android.synthetic.main.fragment_venues.*
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 28,August,2020
 */
class VenuesFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var factory: ViewModelProviderFactory
    private lateinit var venuesViewModel: VenuesViewModel
    private lateinit var locationViewModel: LocationViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        venuesViewModel = viewModelProvider(factory)
        locationViewModel = activityViewModelProvider(factory)
        adapter = VenuesAdapter { holder ->
            venuesViewModel.observeClicks(holder.observe())
        }
        venuesViewModel.loadMoreObserver(adapter.getLoadMoreObservable())

        observe(venuesViewModel.clickObservable, ::observeActions)
        observe(venuesViewModel.venues, ::observeVenues)
        observe(venuesViewModel.isRefreshing, ::observeRefreshing)
        observe(locationViewModel.location, ::observeLocationData)
        observe(venuesViewModel.messageObservable, ::showMessage)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_venues, container, false)


    private fun observeVenues(venues: MutableList<VenueObject>) {
        adapter.addItems(venues)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        loadingIndicator.setOnRefreshListener(this)
    }

    override fun onRefresh() {
        venuesViewModel.refresh()
    }

    private fun observeRefreshing(status: Boolean) {
        if (status) {
            showLoading()
        } else {
            hideLoading()
        }
    }

    private fun observeLocationData(location: Location?) {
        venuesViewModel.setLocation(location)
    }

    private fun hideLoading() {
        if (loadingIndicator.isRefreshing)
            loadingIndicator.post { loadingIndicator?.isRefreshing = false }
    }

    private fun showLoading() {
        if (!loadingIndicator.isRefreshing)
            loadingIndicator.post { loadingIndicator?.isRefreshing = true }
    }

    private fun observeActions(actions: BaseAction) {
        when (actions) {
            is ViewVenueAction -> {
                navigator.toDetail(actions.data)
            }
            else -> {
            }
        }
    }

    companion object {
        fun newInstance(): VenuesFragment = VenuesFragment()
    }

}