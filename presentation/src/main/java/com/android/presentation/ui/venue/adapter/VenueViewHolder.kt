package com.android.presentation.ui.venue.adapter

import android.view.View
import com.android.domain.entity.VenueObject
import com.android.presentation.adapter.BaseViewHolder
import com.android.presentation.adapter.ViewTypeHolder
import com.bumptech.glide.Glide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_venue.view.*

/**
 * Created by hassanalizadeh on 31,August,2020
 */
class VenueViewHolder(
    override val containerView: View
) : BaseViewHolder<VenueObject>(containerView), LayoutContainer {

    override fun getType(): Int = ViewTypeHolder.VENUE_VIEW

    override fun bind(data: VenueObject?) {
        data ?: return

        // Set data to view
        containerView.txt_name.text = data.name
        containerView.txt_address.text = data.address
        containerView.txt_category_name.text = data.category?.name
        Glide.with(containerView.context)
            .load(data.category?.icon)
            .into(containerView.ic_category)

        // Handle click listener
        containerView.root.setOnClickListener {
            mSubject.onNext(ViewVenueAction(data.id))
        }
    }

}