package com.android.presentation.ui.detail

import dagger.Module

/**
 * Created by hassanalizadeh on 31,August,2020
 */
@Module(includes = [DetailViewModelModule::class])
abstract class DetailFragmentModule