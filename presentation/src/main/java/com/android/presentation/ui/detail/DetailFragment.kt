package com.android.presentation.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.common.Constants
import com.android.domain.entity.DetailObject
import com.android.presentation.R
import com.android.presentation.common.extension.observe
import com.android.presentation.common.extension.viewModelProvider
import com.android.presentation.common.view.BaseFragment
import com.android.presentation.common.viewmodel.ViewModelProviderFactory
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_venue_detail.*
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 31,August,2020
 */
class DetailFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory
    private lateinit var viewModel: DetailViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModelProvider(factory)

        observe(viewModel.venueDetail, ::observeVenue)
        observe(viewModel.messageObservable, ::showMessage)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_venue_detail, container, false)


    private fun observeVenue(venue: DetailObject) {
        // Set to view here
        txt_name.text = venue.name
        txt_address.text = venue.address
        Glide.with(this)
            .load(venue.categories[0].icon)
            .into(ic_category)
        txt_category_name.text = venue.categories[0].name
        venue.defaultHour?.let {
            txt_status.text = String.format(resources.getString(R.string.status_is_), it)
        }
        txt_rate.text = String.format(resources.getString(R.string.rate_is_), venue.rating)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        if (savedInstanceState == null)
            arguments?.getString(Constants.ID)?.let { viewModel.detail(it) }
    }


    companion object {

        fun newInstance(id: String): DetailFragment {
            val fragment = DetailFragment()
            val bundle = Bundle()
            bundle.putString(Constants.ID, id)
            fragment.arguments = bundle
            return fragment
        }
    }

}