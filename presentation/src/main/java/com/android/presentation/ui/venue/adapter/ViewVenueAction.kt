package com.android.presentation.ui.venue.adapter

import com.android.presentation.adapter.ActionType
import com.android.presentation.adapter.BaseAction

/**
 * Created by hassanalizadeh on 31,August,2020
 */
data class ViewVenueAction(val data: String) : BaseAction {
    override fun getType(): ActionType = ActionType.VIEW_VENUE
}