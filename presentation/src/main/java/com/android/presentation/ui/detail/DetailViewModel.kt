package com.android.presentation.ui.detail

import androidx.lifecycle.MutableLiveData
import com.android.domain.entity.DetailObject
import com.android.domain.usecase.venue.GetDetailUseCase
import com.android.presentation.common.view.BaseViewModel
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 31,August,2020
 */
class DetailViewModel @Inject constructor(
    private val detailUseCase: GetDetailUseCase
) : BaseViewModel() {

    val venueDetail: MutableLiveData<DetailObject> = MutableLiveData()

    fun detail(id: String) {
        detailUseCase.invoke(id)
            .onError()
            .subscribe({
                venueDetail.value = it
            }, {})
            .track()
    }

}