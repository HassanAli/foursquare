package com.android.presentation.ui.venue

import androidx.lifecycle.ViewModel
import com.android.presentation.common.di.FragmentScope
import com.android.presentation.common.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by hassanalizadeh on 28,August,2020
 */
@Module
abstract class VenuesViewModelModule {

    @Binds
    @IntoMap
    @FragmentScope
    @ViewModelKey(VenuesViewModel::class)
    abstract fun venuesViewModel(venuesViewModel: VenuesViewModel): ViewModel

}