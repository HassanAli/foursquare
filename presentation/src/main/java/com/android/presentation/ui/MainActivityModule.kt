package com.android.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import com.android.presentation.common.di.ActivityScope
import com.android.presentation.common.di.FragmentScope
import com.android.presentation.common.view.BaseActivityModule
import com.android.presentation.location.LocationViewModelModule
import com.android.presentation.location.OnLocationCallback
import com.android.presentation.ui.detail.DetailFragment
import com.android.presentation.ui.detail.DetailFragmentModule
import com.android.presentation.ui.venue.VenuesFragment
import com.android.presentation.ui.venue.VenuesFragmentModule
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import javax.inject.Named

/**
 * Created by hassanalizadeh on 28,August,2020
 */
@Module(
    includes = [
        BaseActivityModule::class,
        LocationViewModelModule::class
    ]
)
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [VenuesFragmentModule::class])
    abstract fun venueFragmentInjector(): VenuesFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [DetailFragmentModule::class])
    abstract fun detailFragmentInjector(): DetailFragment

    @Binds
    @ActivityScope
    abstract fun activity(mainActivity: MainActivity): AppCompatActivity

    @Binds
    @ActivityScope
    abstract fun locationCallback(mainActivity: MainActivity): OnLocationCallback


    @Module
    companion object {

        const val ACTIVITY_FRAGMENT_MANAGER = "MainActivityFragmentManager"

        @JvmStatic
        @Provides
        @Named(ACTIVITY_FRAGMENT_MANAGER)
        @ActivityScope
        fun provideMainActivityFragmentManager(mainActivity: MainActivity): FragmentManager {
            return mainActivity.supportFragmentManager
        }

        @Provides
        @JvmStatic
        fun provideLifecycle(mainActivity: MainActivity): Lifecycle {
            return mainActivity.lifecycle
        }
    }

}