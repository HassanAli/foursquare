package com.android.presentation.ui.venue

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.android.common.extension.orZero
import com.android.domain.entity.VenueObject
import com.android.domain.entity.VenuesObject
import com.android.domain.usecase.invoke
import com.android.domain.usecase.venue.GetVenuesUseCase
import com.android.domain.usecase.venue.LoadMoreVenuesUseCase
import com.android.domain.usecase.venue.RefreshVenuesUseCase
import com.android.presentation.adapter.ActionType
import com.android.presentation.adapter.BaseAction
import com.android.presentation.adapter.LoadMoreState
import com.android.presentation.common.extension.map
import com.android.presentation.common.view.BaseViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 28,August,2020
 */
class VenuesViewModel @Inject constructor(
    private val getVenues: GetVenuesUseCase,
    private val refreshVenuesUseCase: RefreshVenuesUseCase,
    private val loadMoreVenuesUseCase: LoadMoreVenuesUseCase
) : BaseViewModel() {

    private val _venues: LiveData<VenuesObject> =
        LiveDataReactiveStreams.fromPublisher(getVenues.invoke())
    val venues: LiveData<MutableList<VenueObject>> = _venues.map { it.venues.toMutableList() }
    val isRefreshing = MutableLiveData<Boolean>()
    val clickObservable = MutableLiveData<BaseAction>()
    val location = MutableLiveData<Location?>()
    private val locationObserver: Observer<Location?> = Observer { refresh() }

    init {
        isRefreshing.value = true
        location.observeForever(locationObserver)
    }


    fun refresh() {
        location.value?.let {
            refresh(it)
        } ?: run {
            isRefreshing.value = false
        }
    }

    private fun refresh(location: Location) {
        isRefreshing.postValue(true)
        refreshVenuesUseCase.invoke(location.latitude to location.longitude)
            .doOnEvent { isRefreshing.value = false }
            .onError()
            .subscribe()
            .track()
    }

    private fun loadMoreVenues(onNext: () -> Unit) {
        loadMoreVenuesUseCase.invoke()
            .doOnEvent { onNext.invoke() }
            .onError()
            .subscribe()
            .track()
    }

    fun setLocation(location: Location?) {
        this.location.value = location
    }

    fun loadMoreObserver(loadMoreObservable: PublishSubject<LoadMoreState>) {
        loadMoreObservable.subscribe {
            if (it == LoadMoreState.LOAD)
                if (_venues.value?.venues?.size.orZero() >= _venues.value?.totalCount.orZero())
                    loadMoreObservable.onNext(LoadMoreState.FINISH)
                else
                    loadMoreVenues { loadMoreObservable.onNext(LoadMoreState.NOT_LOAD) }
        }.track()
    }

    /**
     * @param actions contains all of the action we have for each ad items in view
     * */
    fun observeClicks(actions: Observable<BaseAction>) {
        actions.subscribe {
            when (it.getType()) {
                ActionType.VIEW_VENUE -> {
                    clickObservable.value = it
                }
            }
        }.track()
    }

    override fun onCleared() {
        super.onCleared()
        location.removeObserver(locationObserver)
    }

}