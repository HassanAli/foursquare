package com.android.presentation.ui

import android.content.Intent
import android.location.Location
import android.os.Bundle
import com.android.presentation.R
import com.android.presentation.common.extension.viewModelProvider
import com.android.presentation.common.view.BaseActivity
import com.android.presentation.common.viewmodel.ViewModelProviderFactory
import com.android.presentation.location.LocationManager
import com.android.presentation.location.LocationViewModel
import com.android.presentation.location.OnLocationCallback
import com.android.presentation.ui.venue.VenuesFragment
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 28,August,2020
 */
class MainActivity : BaseActivity(), OnLocationCallback {

    @Inject
    lateinit var locationManager: LocationManager

    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var locationViewModel: LocationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        locationViewModel = viewModelProvider(factory)

        if (savedInstanceState == null)
            addFragment(R.id.fragmentContainer, VenuesFragment.newInstance())

        locationManager.enable()
    }

    override fun onNewLocation(location: Location?) {
        locationViewModel.setLocation(location)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        locationManager.onActivityResult(requestCode, resultCode)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        locationManager.onRequestPermissionsResult(requestCode, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}