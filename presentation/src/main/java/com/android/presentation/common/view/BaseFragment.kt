package com.android.presentation.common.view

import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.android.presentation.R
import com.android.presentation.adapter.BaseRecyclerAdapter
import com.android.presentation.common.extension.linearLayout
import com.android.presentation.common.extension.toast
import com.android.presentation.common.navigator.Navigator
import com.android.presentation.common.utils.Utils
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_venues.*
import javax.inject.Inject

/**
 * Base class for all fragments.
 */
abstract class BaseFragment : DaggerFragment() {

    @Inject
    protected lateinit var activityContext: Context

    @Inject
    protected lateinit var navigator: Navigator

    protected lateinit var adapter: BaseRecyclerAdapter

    fun showMessage(message: MessageData) {
        if (message.message != null) {
            showMessage(message.message!!)
        } else if (message.resource != null) {
            showMessage(message.resource!!)
        }
    }

    fun showMessage(@StringRes resourceId: Int) {
        showMessage(getString(resourceId))
    }

    fun showMessage(message: String) {
        activityContext.toast(message)
    }

    private fun setupRecyclerView() {
        recyclerView?.linearLayout(
            context = activityContext,
            spacing = Utils.convertPxToDp(activityContext, 8f).toInt()
        )
    }

    private fun setupAdapter() {
        recyclerView?.adapter = adapter
    }

    private fun setSwipeColor() {
        loadingIndicator?.setColorSchemeColors(
            ContextCompat.getColor(activityContext, R.color.colorAccent)
        )
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        setSwipeColor()
        setupRecyclerView()
        setupAdapter()
    }

}