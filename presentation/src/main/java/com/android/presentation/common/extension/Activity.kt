package com.android.presentation.common.extension

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import com.android.common.Constants

fun FragmentActivity.requestPermission(requestCode: Int, vararg permissions: String) {
    ActivityCompat.requestPermissions(this, permissions, requestCode)
}

fun FragmentActivity.checkAppPermission(permission: String): Boolean =
    ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

fun FragmentActivity?.openAppPermissionSetting() {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    val uri = Uri.fromParts("package", this?.applicationContext?.packageName, null)
    intent.data = uri
    this?.startActivityForResult(intent, Constants.RC_APP_SETTING)
}