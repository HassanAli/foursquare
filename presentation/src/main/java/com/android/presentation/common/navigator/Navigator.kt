package com.android.presentation.common.navigator

import androidx.fragment.app.FragmentManager
import com.android.presentation.R
import com.android.presentation.common.extension.addFragment
import com.android.presentation.common.extension.replaceFragment
import com.android.presentation.ui.MainActivityModule
import com.android.presentation.ui.detail.DetailFragment
import javax.inject.Inject
import javax.inject.Named

/**
 * Handle all navigation in here.
 *
 * @param fragmentManager is [MainActivity]'s fragmentManager.
 */
class Navigator @Inject constructor(
    @Named(MainActivityModule.ACTIVITY_FRAGMENT_MANAGER)
    val fragmentManager: FragmentManager
) {

    fun toDetail(id: String) {
        fragmentManager.addFragment(
            R.id.fragmentContainer,
            DetailFragment.newInstance(id),
            true
        )
    }
}