package com.android.presentation.location

import androidx.lifecycle.ViewModel
import com.android.presentation.common.di.ActivityScope
import com.android.presentation.common.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by hassanalizadeh on 01,September,2020
 */
@Module
abstract class LocationViewModelModule {
    @Binds
    @IntoMap
    @ActivityScope
    @ViewModelKey(LocationViewModel::class)
    abstract fun provideLocationViewModel(locationViewModel: LocationViewModel): ViewModel
}