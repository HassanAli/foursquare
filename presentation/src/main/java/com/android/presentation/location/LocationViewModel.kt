package com.android.presentation.location

import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.android.presentation.common.view.BaseViewModel
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 01,September,2020
 */
class LocationViewModel @Inject constructor() : BaseViewModel() {

    val location = MutableLiveData<Location?>()

    fun setLocation(loc: Location?) {
        location.value = loc
    }

}