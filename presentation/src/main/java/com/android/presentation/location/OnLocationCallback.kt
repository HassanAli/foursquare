package com.android.presentation.location

import android.location.Location


interface OnLocationCallback {
    fun onNewLocation(location: Location?)
}
