package com.android.presentation.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.android.common.Constants
import com.android.presentation.R
import com.android.presentation.common.extension.checkAppPermission
import com.android.presentation.common.extension.requestPermission
import com.android.presentation.common.extension.toast
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import javax.inject.Inject

/**
 * https://developer.android.com/topic/libraries/architecture/lifecycle
 * https://proandroiddev.com/android-building-self-contained-lifecycle-aware-testable-components-63f25474646f
 * https://github.com/googlesamples/android-play-location/blob/master/LocationUpdates/app/src/main/java/com/google/android/gms/location/sample/locationupdates/MainActivity.java
 */

class LocationManager @Inject constructor(
    private val activity: AppCompatActivity,
    private val lifecycle: Lifecycle,
    private val callback: OnLocationCallback
) : LifecycleObserver {

    private var enabled = false
    private var forceDetectLocation: Boolean = false
    private var locationIsOn: Boolean = false
    private var timeoutHandler: Handler = Handler()
    private var currentLocation: Location? = null

    private lateinit var settingsClient: SettingsClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationSettingsRequest: LocationSettingsRequest

    init {
        lifecycle.addObserver(this)
    }

    fun enable(
        forceDetectLocation: Boolean = true
    ) {
        this.forceDetectLocation = forceDetectLocation

        val manager =
            activity.getSystemService(Context.LOCATION_SERVICE) as android.location.LocationManager

        locationIsOn =
            activity.checkAppPermission(FINE_LOCATION) &&
                    (manager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER) ||
                            manager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER))

        enabled = true

        if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED) && (locationIsOn || forceDetectLocation))
            start()
    }

    fun disable() {
        stop()
        enabled = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        settingsClient = LocationServices.getSettingsClient(activity)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        locationRequest = LocationRequest().apply {
            interval = LOCATION_REQUEST_INTERVAL
            fastestInterval = LOCATION_REQUEST_FASTEST_INTERVAL
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        locationSettingsRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .build()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult?) {
                super.onLocationResult(result)
                newLocation(result?.lastLocation)
                // stopLocationUpdates()
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        if (enabled && (locationIsOn || forceDetectLocation)) {
            detectLocation()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {
        if (enabled) {
            stopLocationUpdates()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        lifecycle.removeObserver(this)
    }

    private fun detectLocation() {
        if (activity.checkAppPermission(FINE_LOCATION))
            startLocationUpdates()
        else
            requestPermissions()
    }

    private fun requestPermissions() {
        activity.requestPermission(Constants.RC_PERMISSION_LOCATION, FINE_LOCATION)
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        settingsClient.checkLocationSettings(locationSettingsRequest)
            .addOnSuccessListener {
                fusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
                timeoutHandler.postDelayed(noLocationRunnable, LOCATION_REQUEST_TIMEOUT)
            }
            .addOnFailureListener {
                when ((it as ApiException).statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        (it as ResolvableApiException).startResolutionForResult(
                            activity,
                            Constants.RC_CHECK_SETTINGS
                        )
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        activity.toast(activity.getString(R.string.inadequate_location_settings))
                    }
                }
            }
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
            .addOnCompleteListener {}
        timeoutHandler.removeCallbacks(noLocationRunnable)
    }

    @SuppressLint("MissingPermission")
    private val noLocationRunnable: Runnable = Runnable {
        fusedLocationClient.lastLocation
            .addOnSuccessListener {
                newLocation(it)
                stopLocationUpdates()
            }
            .addOnFailureListener {
                newLocation(null)
                stopLocationUpdates()
            }
    }

    private fun newLocation(location: Location?) {
        when {
            currentLocation == null -> {
                currentLocation = location
            }
            location == null -> {
            }
            else -> {
                val array = FloatArray(size = 1)
                Location.distanceBetween(
                    currentLocation!!.latitude,
                    currentLocation!!.longitude,
                    location.latitude,
                    location.longitude,
                    array
                )

                if (array[0] < 100) return
                currentLocation = location
            }
        }

        callback.onNewLocation(location)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int) {
        when (requestCode) {
            Constants.RC_CHECK_SETTINGS -> {
                if (resultCode == AppCompatActivity.RESULT_OK)
                    detectLocation()
                else if (resultCode == AppCompatActivity.RESULT_CANCELED)
                    disable()
            }
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, grantResults: IntArray) {
        if (requestCode == Constants.RC_PERMISSION_LOCATION &&
            grantResults.isNotEmpty() &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            startLocationUpdates()
        } else if (shouldShowRequestPermissionRationale(activity, FINE_LOCATION)) {
            disable()
        }
    }

    companion object {
        const val LOCATION_REQUEST_TIMEOUT: Long = 30000
        const val LOCATION_REQUEST_INTERVAL: Long = 50000
        const val LOCATION_REQUEST_FASTEST_INTERVAL: Long = LOCATION_REQUEST_INTERVAL / 2
        const val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    }

}
