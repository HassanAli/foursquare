package com.android.domain

import com.android.domain.entity.DetailObject
import com.android.domain.entity.VenuesObject
import com.android.domain.executor.transformer.*
import dagger.Binds
import dagger.Module

/**
 * Created by hassanalizadeh on 27,August,2020
 */
@Module
abstract class DomainModule {

    @Binds
    abstract fun completableTransformer(
        transformer: AsyncCTransformer
    ): CTransformer

    @Binds
    abstract fun venuesTransformer(
        transformer: AsyncFTransformer<VenuesObject>
    ): FTransformer<VenuesObject>

    @Binds
    abstract fun detailTransformer(
        transformer: AsyncSTransformer<DetailObject>
    ): STransformer<DetailObject>

}