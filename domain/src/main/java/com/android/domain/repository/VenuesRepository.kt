package com.android.domain.repository

import com.android.domain.entity.DetailObject
import com.android.domain.entity.VenuesObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by hassanalizadeh on 29,August,2020
 */
interface VenuesRepository {
    fun venues(): Flowable<VenuesObject>
    fun loadVenues(param: Pair<Double, Double>): Completable
    fun loadMoreVenues(): Completable
    fun detail(id: String): Single<DetailObject>
}