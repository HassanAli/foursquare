package com.android.domain.entity

/**
 * Created by hassanalizadeh on 29,August,2020
 */
data class CategoryObject(
    val name: String,
    val icon: String?
)