package com.android.domain.entity

/**
 * Created by hassanalizadeh on 31,August,2020
 */
data class VenuesObject(
    val totalCount: Int,
    val venues: List<VenueObject>
): DomainObject