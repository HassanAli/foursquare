package com.android.domain.entity

/**
 * Created by hassanalizadeh on 31,August,2020
 */
data class DetailObject(
    val id: String,
    val name: String,
    val phone: String?,
    val address: String,
    val categories: List<CategoryObject>,
    val verified: Boolean,
    val rating: Double,
    val description: String?,
    val defaultHour: String?
) : DomainObject