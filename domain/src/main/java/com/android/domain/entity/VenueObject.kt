package com.android.domain.entity

/**
 * Created by hassanalizadeh on 29,August,2020
 */
data class VenueObject(
    val id: String,
    val name: String,
    val address: String?,
    val category: CategoryObject?
) : DomainObject