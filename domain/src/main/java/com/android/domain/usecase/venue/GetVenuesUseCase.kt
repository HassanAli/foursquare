package com.android.domain.usecase.venue

import com.android.domain.entity.VenuesObject
import com.android.domain.executor.transformer.FTransformer
import com.android.domain.repository.VenuesRepository
import com.android.domain.usecase.UseCaseFlowable
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 30,August,2020
 */
class GetVenuesUseCase @Inject constructor(
    private val repository: VenuesRepository,
    private val transformer: FTransformer<VenuesObject>
) : UseCaseFlowable<VenuesObject, Unit>() {

    override fun execute(param: Unit): Flowable<VenuesObject> {
        return repository.venues()
            .compose(transformer)
    }

}