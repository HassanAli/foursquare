package com.android.domain.usecase.venue

import com.android.domain.entity.DetailObject
import com.android.domain.executor.transformer.STransformer
import com.android.domain.repository.VenuesRepository
import com.android.domain.usecase.UseCaseSingle
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 31,August,2020
 */
class GetDetailUseCase @Inject constructor(
    private val repository: VenuesRepository,
    private val transformer: STransformer<DetailObject>
): UseCaseSingle<DetailObject, String>() {

    override fun execute(param: String): Single<DetailObject> {
        return repository.detail(param)
            .compose(transformer)
    }

}