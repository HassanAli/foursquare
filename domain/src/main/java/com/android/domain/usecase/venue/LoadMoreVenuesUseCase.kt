package com.android.domain.usecase.venue

import com.android.domain.executor.transformer.CTransformer
import com.android.domain.repository.VenuesRepository
import com.android.domain.usecase.UseCaseCompletable
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by hassanalizadeh on 30,August,2020
 */
class LoadMoreVenuesUseCase @Inject constructor(
    private val repository: VenuesRepository,
    private val transformer: CTransformer
) : UseCaseCompletable<Unit>() {

    override fun execute(param: Unit): Completable {
        return repository.loadMoreVenues()
            .compose(transformer)
    }

}