package com.android.domain.usecase.venue

import com.android.common_test.TestUtil
import com.android.common_test.transformer.TestFTransformer
import com.android.data.entity.mapper.map
import com.android.domain.entity.VenuesObject
import com.android.domain.repository.VenuesRepository
import com.android.domain.usecase.invoke
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

/**
 * Created by hassanalizadeh on 30,August,2020
 */
@RunWith(JUnit4::class)
class GetVenuesUseCaseTest {

    @Mock
    private lateinit var repository: VenuesRepository
    private lateinit var useCase: GetVenuesUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        useCase = GetVenuesUseCase(repository, TestFTransformer())
    }


    @Test
    fun execute() {
        // GIVEN
        val expected = TestUtil.firstVenueFromRemote().id
        doReturn(Flowable.just(VenuesObject(0, mutableListOf(TestUtil.firstVenueFromRemote().map().map()))))
            .whenever(repository).venues()

        // WHEN
        useCase.invoke()
            .test()
            .assertValue {
                it.venues[0].id == expected
            }
            .assertComplete()

        // THEN
        verify(repository).venues()
    }

}