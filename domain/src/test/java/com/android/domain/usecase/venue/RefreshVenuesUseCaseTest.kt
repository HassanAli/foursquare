package com.android.domain.usecase.venue

import com.android.common_test.transformer.TestCTransformer
import com.android.domain.repository.VenuesRepository
import com.android.domain.usecase.invoke
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by hassanalizadeh on 30,August,2020
 */
@RunWith(JUnit4::class)
class RefreshVenuesUseCaseTest {

    @Mock
    private lateinit var repository: VenuesRepository
    private lateinit var useCase: RefreshVenuesUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        useCase = RefreshVenuesUseCase(repository, TestCTransformer())
    }


    @Test
    fun execute() {
        // GIVEN
        Mockito.doReturn(Completable.complete()).whenever(repository).loadVenues(any())

        // WHEN
        useCase.invoke(any())
            .test()
            .assertComplete()

        // THEN
        Mockito.verify(repository).loadVenues(any())
    }

}