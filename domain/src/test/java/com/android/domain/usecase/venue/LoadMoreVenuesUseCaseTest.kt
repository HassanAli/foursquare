package com.android.domain.usecase.venue

import com.android.common_test.transformer.TestCTransformer
import com.android.domain.repository.VenuesRepository
import com.android.domain.usecase.invoke
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by hassanalizadeh on 30,August,2020
 */
@RunWith(JUnit4::class)
class LoadMoreVenuesUseCaseTest {

    @Mock
    private lateinit var repository: VenuesRepository
    private lateinit var useCase: LoadMoreVenuesUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        useCase = LoadMoreVenuesUseCase(repository, TestCTransformer())
    }


    @Test
    fun execute() {
        // GIVEN
        Mockito.doReturn(Completable.complete()).whenever(repository).loadMoreVenues()

        // WHEN
        useCase.invoke()
            .test()
            .assertComplete()

        // THEN
        Mockito.verify(repository).loadMoreVenues()
    }

}