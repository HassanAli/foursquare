package com.android.common.extension

/**
 * Created by hassanalizadeh on 31,August,2020
 */
fun Int?.orZero(): Int = this ?: 0